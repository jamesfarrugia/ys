# YieldStreet Sr. BE Engineer take-home challenge
James Farrugia

## 1. Overview
The application is a Spring boot app with webflux.  Controllers are defined in a functional-reactive
API route in ApiControllerConfig.  The reactive approach is followed all the way down to the services.
A functional approach is kept as much as possible however the occasional @Autowired service in the
class scope (vs the method/function scope) is still found.

The reactive approach is intended to allow us to handle load elastically with heavy processing being
scheduled separately from request handlers - as such we are not limited by a fixed pool of threads
to handle the number of concurrent requests.

## 2. Building
Building the app is possible following the usual approaches:
 1. `mvn clean package` which produces a runnable jar file in `./target`
 2. `mvn spring-boot:build-image` which apart from building the jar file, will also build a docker image for the application
 
## 3. Running
Running the app is possible in multiple ways as well, but still keeping it straight-forward.  All
options will utilise the local port `8080` and will bind to `localhost`.  The application API is
always available at `http://localhost:8080/user/accreditation`. 

 1. `mvn spring-boot:run` will compile, package, and run the application;
 2. `java -jar ./target/accreditation-0.0.1-SNAPSHOT.jar` will launch the application via a normal java run, assuming that the application was already built (see 2.1)
 3. `docker run -it -p 8080:8080 accreditation:0.0.1-SNAPSHOT` will launch the app within docker, assuming that the image was built (see 2.2)
 4. `docker-compose up` will launch the application along with prometheus and grafana in a docker-compose environment, assuming the image was already built (see 2.2)

### Access
The application primarily offers the API at `http://localhost:8080/user/accreditation`.  This API handles user accreditation requests and replies with the appropriate response as per spec.

The API is documented at `http://localhost:8080/swagger-ui.html` - all the schemas and sample requests are presented here.

#### API Notes
The requests follow the spec in that only application/json content is accepted and the shape of the request assumes that all fields are required (including at least one document).  Validation is adhered to and any mismatches will reject the processing of the request.

Fields such as the document mime type and base64 content are also checked for validity.

### Monitoring
In the case when the application is launched via `docker-compose`, an instance of prometheus and grafana are also launched.  Everything is configured so that the application is immediately monitored.  Note that launching in any other way will also expose the metrics endpoint, however this will not be rendered as graphs.

The prometheus console is available at `http://localhost:9090` while grafana is at `http://localhost:3000/`.  Credentials for grafana are `admin/admin`.  Prometheus is used to scrape the application metrics on the `http://localhost:8080/actuator/prometheus` endpoint, on which we also expose an extra metric for accreditation rates.

In grafana, the *Accreditation Metrics* board presents a simple graph of approved vs rejected accreditation requests, which given the random operation of the service, one would expect to have a similar distribution.

Additionally we have the *Application Stats* board which is a fairly standard board that presents metrics such as memory usage, uptime etc.  One should note the consistent number of threads even when requesting a larger number of accreditation requests.

## Application design
The application is split into two main concerns, the (business) domain and the infrastructure.  The domain, as one can expect, contains business logic such as decision making for accreditations, domain specific models and values.  Each "sub-domain" is put into relevant services via interfaces so we keep implementations away between concerns.  The implementation is fairly simple, even trivial in some cases, due to the problem being addressed, however where possible a more robust reactive and function approach was followed (as opposed to a trivial `random.nextBoolean()`).

On the infrastructure side we have application configs, including the API route configuration.  The API route will functionally match requests and forward them to a handler.  In this case there is one concrete handler, which is extending a validator handler, as well as a generic error handler.  The concrete handler primarily accepts a validated request which it then transforms and forwards to the accreditation service.  The mapping and validation is done hand in hand with the super-class validator handler.

The validator handler is responsible for receiving server requests, from which it extracts the body and then passes it through the annotation-based validator which is the default in Spring.  Additionally we also have two extra annotations and validators to specifically handle our mime type and base64 types.

In these two handlers we have the common error handler.  The main scope of the error handler is to accept throwable types from Error Monos, convert them to errors objects, seek the appropriate HTTP status to use and finally reply with an HTTP error response and message.  The Error object is mainly used as a DTO and View for all erorrs.

JSON validation, serilisation, and deserialisation are handled by Jackson which is the default within the spring context.

## Discussion
The implementation of this app was driven by various factors and constraints which mainly revolve around available time as well as the small scope.  Given that we have virtually no IO bound operations (such as DB access, RPC or REST clients etc), we can safely work in a block-free system, in which case a reactive approach could feel overkill (at first).

However, given the scenario where we want to swap out the randomiser for an actual service, we are prepared to handle more requests.  Expectedly, in case the load goes beyond what one instance can take, we would need some infrastructural level of scalability, such as kubernetes which spawns more instances of this component.  As it is right now, we have zero restrictions to scaling, since all random decisions are made locally and statelessly.  If however we need to communicate with some other service, or we want a proper accreditation system that logs and maintains individual accreditation requests, we should look into a more formal event driven/sourced system and consider CQRS so that we can treat the business request of accreditation as a proper command.  We must also consider the case of the same user submitting multiple accreditation requests - in such a scenario we must then again consider the case of either receiving the same accreditation request with the same files etc, or a different accreditation request (with different documents) from the same user.  

In that situation we would need to manage some common repository of requests (i.e. multiple instances of these API must be aware of the requests the other instances handled) to make sure we do not submit duplicates, while at the same time technically we need to handle API evolution (via versioning for example) so that we can start moving over to a more idempotent approach.  This could then require sharding to handle heavier loads, or ideally use idempotency keys, etc.

Similarly we would want to handle the case when our backing services fail.  In the test cases for this app, we do consider the case of the randomiser failing, even though this is not a documented case for the `java.util.Random.nextBoolean`.  This is needed however since we might move over to a service that could fail.  In that case then again, we must consider failing over to another service, retrying the request (where idempotency comes into play again) or simply bubbling up the failure to the user.
