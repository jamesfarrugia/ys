package com.jf.accreditation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main Spring boot application 
 * @author james
 *
 */
@SpringBootApplication
public class AccreditationApplication {

	/**
	 * Standard main method
	 * @param args command line args
	 */
	public static void main(String[] args) {
		SpringApplication.run(AccreditationApplication.class, args);
	}

}
