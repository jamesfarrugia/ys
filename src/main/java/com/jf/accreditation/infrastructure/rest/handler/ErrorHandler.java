package com.jf.accreditation.infrastructure.rest.handler;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.jf.accreditation.infrastructure.rest.model.Error;

import reactor.core.publisher.Mono;

/**
 * API Handler for error situations.  This handler receives all cases where we encounter and error
 * and want to reply with the appropriate http response code and message.  This handler is also 
 * responsible for mapping from exceptions into appropriate http status codes.
 * 
 * @author james
 *
 */
@Service
public class ErrorHandler {

	/**
	 * Transforms the passed throwable into a reactive server response representing an error
	 * response
	 * 
	 * @param throwable the throwable to reply with
	 * @return a Mono of {@link ServerResponse} for the passed throwable
	 */
	public Mono<ServerResponse> throwableError(final Throwable throwable) {
		return Mono.just(throwable).transform(this::getResponse);
	}

	/**
	 * Maps the passed throwable mono into the appropriate server response by mapping it into an
	 * Error Object (passing an error mapping function) and then building a server response from
	 * the resulting error (http code and message).
	 * 
	 * @param <T>
	 * @param monoError
	 * @return a mono of {@link ServerResponse} for the passed error
	 */
	private <T extends Throwable> Mono<ServerResponse> getResponse(final Mono<T> monoError) {
		return monoError.
				map(t -> Error.fromThrowable(t, (x) -> getStatus(x))).
				flatMap(e -> ServerResponse.status(e.getHttpStatus()).bodyValue(e));
	}
	
	/**
	 * Map form a {@link Throwable} into an {@link HttpStatus}
	 * @param error the throwable to map
	 * @return the status to be used for such a given throwable
	 */
	private HttpStatus getStatus(final Throwable error) {
		if (error instanceof IllegalArgumentException) {
			return HttpStatus.BAD_REQUEST;
		} else {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
	}
}
