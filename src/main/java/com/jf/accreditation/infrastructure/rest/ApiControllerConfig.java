package com.jf.accreditation.infrastructure.rest;

import static org.springframework.web.reactive.function.server.RequestPredicates.contentType;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

import org.springdoc.core.annotations.RouterOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.jf.accreditation.domain.model.UserAccreditationRequest;
import com.jf.accreditation.domain.model.UserAccreditationResponse;
import com.jf.accreditation.infrastructure.rest.handler.UserAccreditationHandler;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * Configuration for the API controller based on the reactive router, as opposed to "traditional"
 * annotation driven controllers.  Reactive controllers are possible, however the functional
 * approach allows for more purity with no side effects etc.
 * 
 * @author james
 *
 */
@Configuration
public class ApiControllerConfig {

	/**
	 * Main router function for the (only) api route at <pre>/user/accreditation</pre>
	 * The route is tested against the content type predicate so we only accept the JSON requests
	 * and then we ask the handler to reactively handle the request.
	 * 
	 * The @RouterOperation annotation allows us to document the API using OpenAPI standard - 
	 * the OK and bad request or not found responses are explicitly documented.  In our implementation
	 * the 404 is unlikely to be hit on this API since we do no check for the existence of the User
	 * ID
	 * 
	 * @param handler the user accreditation handler to use for handling the API requests
	 * @return the configured route
	 */
	@Bean
	@RouterOperation(
		operation = @Operation(operationId = "userAccreditation", 
								summary = "Process proof of accreditation for a user", 
								tags = {"Accreditation"}, 
								
		requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = UserAccreditationRequest.class))),
		responses = {
					@ApiResponse(responseCode = "200", description = "Successful operation", 
								content = @Content(schema = @Schema(implementation = UserAccreditationResponse.class))),
					@ApiResponse(responseCode = "400", description = "Invalid Accreditation request supplied"),
					@ApiResponse(responseCode = "404", description = "User not found") 
					}
	))
	public RouterFunction<ServerResponse> apiRoutes(@Autowired UserAccreditationHandler handler) {
		return route()
				.POST("/user/accreditation", contentType(MediaType.APPLICATION_JSON), handler::handleRequest)
				.build();
	}
}