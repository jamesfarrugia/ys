package com.jf.accreditation.infrastructure.rest.model;

import java.util.function.Function;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

/**
 * General error response for technical failures
 * @author james
 *
 */
@Getter
public class Error extends Response {
	/** Http Status mainly used for error handling information, but not passed as value to 
	 * the client - this is then implied in the HTTP headers */
	@JsonIgnore
	private final HttpStatus httpStatus;
	
	/** Human readable error message.  We can extend this to include more detailed error info
	 * etc.*/
	@JsonProperty("message")
	private final String message;
	
	/**
	 * Constructor for the error, which is implemented as opposed to being done by Lombok due to
	 * the explicit super call indicating this as a non-successful technical response
	 * @param httpStatus HTTP Status for this error
	 * @param message error message
	 */
	public Error(HttpStatus httpStatus, String message) {
		super(false);
		this.httpStatus = httpStatus;
		this.message = message;
	}
	
	/**
	 * Static instantiation of an error for a more readable line.  This is more elaborate than
	 * the constructor as it requires a function to be passed which will determine the HTTP error
	 * code from the given throwable.  This keeps our error mapping as an ErrorHandler concern
	 * and we can just pass a function that calls that mapping function.
	 *  
	 * @param t the throwable from which to build an error
	 * @param codeMapper a function to get us the HTTP status from the throwable
	 * @return and error with the message from the throwable and the HTTP code as mapped
	 */
	public static Error fromThrowable(Throwable t, Function<Throwable, HttpStatus> codeMapper) {
		return new Error(codeMapper.apply(t), t.getMessage());
	}
}
