package com.jf.accreditation.infrastructure.rest.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.codec.binary.Base64;

/**
 * ContraintValidator for the Base64 type input.  The validator checks that a passed string is
 * valid within the Base64 character set, but does not verify that the decoded content is valid in
 * the passed type, however it assures us that we can safely decode further downstream.
 * 
 * @author james
 *
 */
public class Base64Validator implements ConstraintValidator<IsBase64, String> {

	@Override
	public boolean isValid(String base64Content, ConstraintValidatorContext cxt) {
		/* Check that we at least have content to process and the it is a valid Base64 string */
		return base64Content != null && Base64.isBase64(base64Content);
	}

}