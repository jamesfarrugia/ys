package com.jf.accreditation.infrastructure.rest.handler;

import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Validator;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.jf.accreditation.domain.model.UserAccreditationRequest;
import com.jf.accreditation.domain.model.UserAccreditationResponse;
import com.jf.accreditation.domain.service.UserAccreditationService;
import com.jf.accreditation.infrastructure.rest.validation.AbstractValidationHandler;

import reactor.core.publisher.Mono;

/**
 * API Handler for the User Accreditation.  The handler will receive a request and ask the user 
 * accreditation service to process the request.  The response is then automatically mapped, and any
 * business errors are automatically mapped into proper error responses.
 * 
 * @author james
 *
 */
@Component
public class UserAccreditationHandler extends AbstractValidationHandler<UserAccreditationRequest, Validator>{
	
	/** Service to process accreditation */
	private UserAccreditationService userAccreditationService;
	
	/**
	 * Constructor receiving required services and initialising itself as a validated handler, 
	 * requiring the default Spring validator and validating the annotated {@link UserAccreditationRequest}
	 * class.
	 * 
	 * @param validator validator implementation
	 * @param errorHandler error handler
	 * @param userAccreditationService user accreditation service implementation
	 */
	public UserAccreditationHandler(
			@Autowired Validator validator,
			@Autowired ErrorHandler errorHandler,
			@Autowired UserAccreditationService userAccreditationService) {
		super(UserAccreditationRequest.class, validator, errorHandler);
		this.userAccreditationService = userAccreditationService;
	}
	
	/**
	 * Main processing method which forwards the request to the accreditation service and maps the
	 * response to the proper API response class.
	 */
	@Override
	protected Mono<ServerResponse> processRequest(Mono<UserAccreditationRequest> request, ServerRequest originalRequest) {
		return request.
				flatMap(userAccreditationService::doProcessAccreditationRequest).
				transform(this.monoResponse(UserAccreditationResponse.class));
	}
	
	/**
	 * Builds a partial function based on the passed type which will then receive an instance of
	 * the passed type and mapped into an OK Server response, unless it has an error mono in which
	 * case we request the error handler to handle it like the rest of the error situations
	 * 
	 * @param <T> the type of class to map the response to
	 * @param type class of the type of class to map the response to
	 * @return a function that can be invoked by the transform to get a Mono of {@link ServerResponse}
	 */
	private final <T> Function<Mono<T>, Mono<ServerResponse>> monoResponse(Class<?> type) {
		return (mono) -> {
			return mono.flatMap(
					monoed -> ServerResponse.ok().body(Mono.just(monoed), type)).
					onErrorResume(errorHandler::throwableError);
		};
	}
}
