package com.jf.accreditation.infrastructure.rest.validation;

import javax.activation.MimeType;
import javax.activation.MimeTypeParseException;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * ContraintValidator for the mime type input.  The validator is "smart" in the sense that it can
 * determine the correctness of the mime type format, but it cannot check for the true validity
 * of the type itself.  Provided we have an exhaustive list of strictly accepted types within our
 * business domain (such as jpg, pdf, docx) we could then add a further step to verify the 
 * acceptability of such type.
 * 
 * @author james
 *
 */
public class MimeTypeValidator implements ConstraintValidator<IsMimeType, String> {

	@Override
	public boolean isValid(String mimeTypeContent, ConstraintValidatorContext cxt) {
		/* Verify that we have some content at least */
		if (mimeTypeContent == null) {
			return false;
		}
		
		/* Test the validity of the type format, if it fails, it is invalid */
		try {
			new MimeType(mimeTypeContent);
		} catch (MimeTypeParseException e) {
			return false;
		}
		
		/* using a fail-fast/return early pattern, our validation must be true of all other checks 
		 * saw no problem*/
		return true;
	}

}