package com.jf.accreditation.infrastructure.rest.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * The annotated element must have a non-null and valid Base64 string, of which an empty string
 * is included.
 * <p>
 * Supported types are:
 * <ul>
 * <li>{@code String}</li>
 * </ul>
 *
 * @author James Farrugia
 */
@Documented
@Constraint(validatedBy = Base64Validator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface IsBase64 {
	String message() default "Invalid base64 encoding";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
