package com.jf.accreditation.infrastructure.rest.validation;

import java.util.stream.Collectors;

import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.jf.accreditation.infrastructure.rest.handler.ErrorHandler;

import reactor.core.publisher.Mono;

/**
 * Abstract handler that includes request validation and a streamlined error handling for such
 * input validation errors
 * 
 * @author james
 *
 * @param <T> type of class to validate
 * @param <U> validator implementation
 */
public abstract class AbstractValidationHandler<T, U extends Validator> {
	
	/** Error handler to use when building error - protected for use by implementing handlers */
	protected final ErrorHandler errorHandler;
	/** Class being validated in the body */
	private final Class<T> validationClass;
	/** Validator implementation */
	private final U validator;

	/**
	 * Constructor for this validated handler
	 * @param validationClass class to validate
	 * @param validator validator implementation
	 * @param errorHandler error handler
	 */
	protected AbstractValidationHandler(Class<T> validationClass, U validator, ErrorHandler errorHandler) {
		this.validationClass = validationClass;
		this.validator = validator;
		this.errorHandler = errorHandler;
	}

	/**
	 * Main handler method which accepts the server request.  This will grab the body and validate
	 * it against the passed validator.  Any errors are aggregated into a final error response, 
	 * otherwise the request is passed onto the implementing handler.  Error responses are 
	 * automatically handled via the error handler.
	 * 
	 * @param request request to validate and propagate
	 * @return the server response
	 */
	public final Mono<ServerResponse> handleRequest(final ServerRequest request) {
		
		return request.bodyToMono(this.validationClass).flatMap(body -> {
			
			Errors errors = new BeanPropertyBindingResult(body, this.validationClass.getName());
			validator.validate(body, errors);

			if (errors == null || errors.getAllErrors().isEmpty()) {
				return processRequest(Mono.just(body), request);
			} else {
				String message = errors.
						getAllErrors().stream().
						map(e -> e.getDefaultMessage()).
						collect(Collectors.joining(", "));
				
				return Mono.error(new IllegalArgumentException("Invalid request: " + message));
			}
		}).onErrorResume(errorHandler::throwableError);
	}

	/**
	 * Method to be implemented by the implementing handler where the handling of the request can
	 * be safely considered as valid from the point of view of the validator passed by the same
	 * implementing handler.
	 * 
	 * @param body body in the deserialised expected type
	 * @param originalRequest original server request
	 * @return a server response after handling this request
	 */
	abstract protected Mono<ServerResponse> processRequest(Mono<T> body, final ServerRequest originalRequest);
}