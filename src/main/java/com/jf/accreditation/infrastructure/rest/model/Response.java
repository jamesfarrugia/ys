package com.jf.accreditation.infrastructure.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Generic abstract response class that automatically includes the success flag.  Implementing
 * response classes can indicate the success flag on constructor (typically false for errors, true
 * for others).  The success flag here is indicating a technical success, more than a business
 * process success, which is to be managed by proper business responses.
 * 
 * @author james
 *
 */
@AllArgsConstructor
@Getter
public abstract class Response {
	/** True for technical success, false otherwise */
	@JsonProperty(value = "success", access = Access.READ_ONLY)
	protected final boolean success;
}
