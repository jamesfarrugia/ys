package com.jf.accreditation.infrastructure.rest.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * The annotated element must contain a valid mime type string
 * <p>
 * Supported types are:
 * <ul>
 * <li>{@code String}</li>
 * </ul>
 *
 * @author James Farrugia
 */
@Documented
@Constraint(validatedBy = MimeTypeValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface IsMimeType {
	String message() default "Invalid mime type (must be in type/subtype format)";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
