package com.jf.accreditation.domain.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jf.accreditation.domain.model.UserAccreditationRequest;
import com.jf.accreditation.domain.model.UserAccreditationResponse;
import com.jf.accreditation.domain.service.MetricsService;
import com.jf.accreditation.domain.service.RandomiserService;
import com.jf.accreditation.domain.service.UserAccreditationService;

import reactor.core.publisher.Mono;

/**
 * An implementation of the {@link UserAccreditationService} which randomly decides if we approve
 * or reject an accreditation request
 * 
 * @author james
 *
 */
@Service
public class RandomUserAccreditationServiceImpl implements UserAccreditationService {
	/** Service for metrics to track business decisions*/
	private MetricsService metricsService;
	/** A randomiser to randomly decide the outcome*/
	private RandomiserService random;
	
	/**
	 * Constructor which get dependencies passed into it
	 * @param metricsService the metrics service implementation
	 */
	public RandomUserAccreditationServiceImpl(
			@Autowired MetricsService metricsService,
			@Autowired RandomiserService random) {
		this.metricsService = metricsService;
		this.random = random;
	}

	/**
	 * Main processing of an accreditation request.  We first get the accreditation and then we map
	 * it into the required reactive response object
	 */
	public Mono<UserAccreditationResponse> doProcessAccreditationRequest(UserAccreditationRequest request) {
		return getAccreditation(request).
				map(accredited -> new UserAccreditationResponse(accredited));
	}
	
	/**
	 * An accreditation flow which maps out request into a random boolean (which we can easily 
	 * swap out for a proper service) and then functional-reactively will filter out the non
	 * approved accreditations.  Those that pass the filter will hit the approved metric and map
	 * into a TRUE response, otherwise on an empty stream we map onto a stream that provides a FALSE
	 * and we notify the rejected metric.
	 * 
	 * This logic is mostly sound for the given boolean-only scenario.
	 * 
	 * @param request the accreditation request
	 * @return a reactive response indicating true for approved, false for rejected accreditation
	 */
	private Mono<Boolean> getAccreditation(UserAccreditationRequest request) {
		return Mono.just(request).
			map(m -> random.nextBoolean()).
			filter(accredited -> accredited.booleanValue()).
			doOnNext(next -> this.metricsService.onApprovedAccreditation()).
			flatMap(accredited -> Mono.just(accredited)).
			switchIfEmpty(
					Mono.just(Boolean.FALSE).
					doOnNext(next -> this.metricsService.onRejectedAccreditation())
			);
	}
}
