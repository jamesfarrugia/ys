package com.jf.accreditation.domain.service;

/**
 * Service to log business domain metrics
 * @author james
 *
 */
public interface MetricsService {
	/**
	 * Notify metrics on when an accreditation is approved
	 */
	public void onApprovedAccreditation();
	
	/**
	 * Notify metrics on when an accreditation is rejected
	 */
	public void onRejectedAccreditation();
}
