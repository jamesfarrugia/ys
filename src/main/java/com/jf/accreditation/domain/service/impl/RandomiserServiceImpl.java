package com.jf.accreditation.domain.service.impl;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.jf.accreditation.domain.service.RandomiserService;

/**
 * Service to randomise the next boolean
 * @author james
 *
 */
@Service
public class RandomiserServiceImpl implements RandomiserService {
	
	/** Internal randomiser */
	private Random random;
	
	/**
	 * Constructor which initialised the randomiser
	 */
	public RandomiserServiceImpl() {
		this.random = new Random();
	}
	
	@Override
	public Boolean nextBoolean() {
		return Boolean.valueOf(random.nextBoolean());
	}

}
