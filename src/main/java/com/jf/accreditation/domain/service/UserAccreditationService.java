package com.jf.accreditation.domain.service;

import com.jf.accreditation.domain.model.UserAccreditationRequest;
import com.jf.accreditation.domain.model.UserAccreditationResponse;

import reactor.core.publisher.Mono;

/**
 * Service for User Accreditation
 * @author james
 *
 */
public interface UserAccreditationService {
	
	/**
	 * Process an accreditation request and reply with the appropriate reactive response
	 * @param request accreditation request
	 * @return reactive accreditation response
	 */
	public Mono<UserAccreditationResponse> doProcessAccreditationRequest(UserAccreditationRequest request);
}
