package com.jf.accreditation.domain.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jf.accreditation.domain.service.MetricsService;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;


/**
 * Implementation of the metrics service using prometheus gauges and counters.
 * 
 * @author james
 *
 */
@Service
public class PrometheusMetricsServiceImpl implements MetricsService {
	/** Registy of metrics */
	private final MeterRegistry meterRegistry;
	/** Counter for approved accreditations */
	private Counter approvedAccreditationCounter;
	/** Counter for rejected accreditations */
	private Counter rejectedAccreditationCounter;
	
	/**
	 * Constructor for service
	 * @param meterRegistry metrics registry
	 */
	public PrometheusMetricsServiceImpl(@Autowired MeterRegistry meterRegistry) {
		this.meterRegistry = meterRegistry;
		this.doInitCounters();
	}
	
	/**
	 * Initialises the counters we want for our domain
	 */
	private void doInitCounters() {
		approvedAccreditationCounter = Counter.builder("accreditation.requests")
				.tag("type", "approved")
				.description("The number accreditation requests that were approved")
				.register(meterRegistry);
		
		rejectedAccreditationCounter = Counter.builder("accreditation.requests")
				.tag("type", "rejected")
				.description("The number accreditation requests that were rejected")
				.register(meterRegistry);
	}
	
	@Override
	public void onApprovedAccreditation() {
		this.approvedAccreditationCounter.increment();
	}

	@Override
	public void onRejectedAccreditation() {
		this.rejectedAccreditationCounter.increment();
	}

}
