package com.jf.accreditation.domain.service;

/**
 * Service to provide a random boolean
 * @author james
 *
 */
public interface RandomiserService {
	/**
	 * Randomise next boolean value
	 * @return
	 */
	public Boolean nextBoolean();
}
