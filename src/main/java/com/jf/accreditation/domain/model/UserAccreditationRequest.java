package com.jf.accreditation.domain.model;

import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jf.accreditation.infrastructure.rest.validation.IsBase64;
import com.jf.accreditation.infrastructure.rest.validation.IsMimeType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Request for User Accreditation.  All fields are assigned a json property in the snake_notation
 * while all nested structures are declared as static classes within this class. 
 * @author james
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAccreditationRequest {
	
	/** ID of the user making the request which is required */
	@JsonProperty("user_id")
	@NotBlank
	private String userId;
	
	/** Payload for the accreditation which is required */
	@JsonProperty("payload")
	@NotNull
	@Valid
	private AccreditationPayload payload;
	
	/** Payload structure */
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class AccreditationPayload {
		/** Type of accreditation which is required */
		@JsonProperty("accreditation_type")
		@NotNull
		@Valid
		private AccreditationType type;
		
		/** List of documents for proof which cannot be empty or null */
		@JsonProperty("documents")
		@NotEmpty
		@Valid
		private Set<AccreditationDocument> documents;
	}
	
	/** Document structure */
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class AccreditationDocument {
		/** name of document which is required */
		@JsonProperty("name")
		@NotEmpty
		private String name;
		
		/** mime type which is required and has to be in the standard (type/subtype) format */
		@JsonProperty("mime_type")
		@NotEmpty
		@IsMimeType
		private String mimeType;
		
		/** Base64 encoded content which has is required and needs a valid Base64 value */
		@JsonProperty("content")
		@NotEmpty
		@IsBase64
		private String content;
	}
}
