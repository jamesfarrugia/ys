package com.jf.accreditation.domain.model;

/**
 * Fixed accreditation types to support
 * @author james
 *
 */
public enum AccreditationType {
	/** Accreditation by income */
	BY_INCOME
}
