package com.jf.accreditation.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jf.accreditation.infrastructure.rest.model.Response;

import lombok.Getter;

/**
 * Response for the user accreditation.  This mainly indicated the accreditation response in a single
 * flag value.  We use only a lombok getter (along with a final field) to keep this immutable.
 * @author james
 *
 */
@Getter
public class UserAccreditationResponse extends Response {
	/** Flag to indicate accreditation status */
	@JsonProperty("accredited")
	private final Boolean accredited;
	
	/**
	 * User Accreditation Response 
	 * @param accredited flag to indicate accreditation status
	 */
	public UserAccreditationResponse(Boolean accredited) {
		super(true);
		this.accredited = accredited;
	}
	
	/**
	 * User Accreditation Response - Package local only
	 * @param accredited flag to indicate accreditation status
	 */
	UserAccreditationResponse(Boolean accredited, boolean success) {
		super(true);
		this.accredited = accredited;
	}
}
