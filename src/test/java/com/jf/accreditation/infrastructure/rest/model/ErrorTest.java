package com.jf.accreditation.infrastructure.rest.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.function.Function;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.http.HttpStatus;

@JsonTest
public class ErrorTest {

	@Autowired
	private JacksonTester<Error> json;
	
	@Test
	public void testSerialize() throws Exception {
		Error sut = new Error(HttpStatus.NOT_FOUND, "not found");
		
		JsonContent<Error> result = this.json.write(sut);

		assertThat(result).hasJsonPathStringValue("$.message");
		assertThat(result).extractingJsonPathStringValue("$.message").isEqualTo("not found");
		
		assertThat(result).hasJsonPathBooleanValue("$.success");
		assertThat(result).extractingJsonPathBooleanValue("$.success").isEqualTo(Boolean.FALSE);
	}
	
	@Test
	public void testDeserialize() throws Exception {
		String jsonInput = "{\n"
				+ "\"success\": false,\n"
				+ "\"message\": \"error\"\n"
				+ "}";
		
		Error result = this.json.parse(jsonInput).getObject();

		assertThat(result.getMessage()).isEqualTo("error");
		assertThat(result.isSuccess()).isFalse();
	}
	
	@Test
	public void testBehaviourFromThrowable_ShouldPass() {
		@SuppressWarnings("unchecked")
		Function<Throwable, HttpStatus> mock = Mockito.mock(Function.class);
		IllegalArgumentException exception = new IllegalArgumentException();
		
		when(mock.apply(exception)).thenReturn(HttpStatus.BAD_REQUEST);
		Error result = Error.fromThrowable(exception, mock);
		verify(mock).apply(exception);
		
		assertThat(result.getHttpStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
	}
}
