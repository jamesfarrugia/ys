package com.jf.accreditation.infrastructure.rest.validation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class MimeTypeValidatorTest {

	@Test
	public void testValidation_emptyString_shouldPass() {
		MimeTypeValidator validator = new MimeTypeValidator();
		assertFalse(validator.isValid("", null));
	}
	
	@Test
	public void testValidation_validString_shouldPass() {
		MimeTypeValidator validator = new MimeTypeValidator();
		assertTrue(validator.isValid("application/json", null));
	}
	
	@Test
	public void testValidation_invalidString_shouldFail() {
		MimeTypeValidator validator = new MimeTypeValidator();
		assertFalse(validator.isValid("%%!", null));
	}
}
