package com.jf.accreditation.infrastructure.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.validation.Validator;

import com.jf.accreditation.domain.model.UserAccreditationRequest;
import com.jf.accreditation.domain.model.UserAccreditationResponse;
import com.jf.accreditation.domain.service.UserAccreditationService;
import com.jf.accreditation.infrastructure.rest.handler.ErrorHandler;
import com.jf.accreditation.infrastructure.rest.handler.UserAccreditationHandler;

import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
public class ApiIT {
	@InjectMocks
	private UserAccreditationHandler handler;
	
	@Mock 
	private Validator validator;
	@Mock 
	private ErrorHandler errorHandler;
	@Mock
	private UserAccreditationService service;
	
	WebTestClient client;
	
	@BeforeEach
	public void setUp() {
		client = WebTestClient.bindToRouterFunction(new ApiControllerConfig().apiRoutes(handler)).build();
	}

	@Test
	public void testAccreditation_PassedAccreditaiton_ShouldPass() {
		when(service.doProcessAccreditationRequest(any())).
		thenReturn(Mono.just(new UserAccreditationResponse(Boolean.TRUE)));
		
		UserAccreditationRequest accreditationRequest = new UserAccreditationRequest("", null);
		
		client.
			post().
			uri("/user/accreditation").accept(MediaType.APPLICATION_JSON).
			bodyValue(accreditationRequest).
			exchange().
			expectStatus().isOk().
			expectHeader().contentType(MediaType.APPLICATION_JSON).
			expectBody().
			jsonPath("$.success").isBoolean().
			jsonPath("$.success").isEqualTo(Boolean.TRUE).
			jsonPath("$.accredited").isBoolean().
			jsonPath("$.accredited").isEqualTo(Boolean.TRUE);
	}
	
	@Test
	public void testAccreditation_FailedAccreditaiton_ShouldPass() {
		when(service.doProcessAccreditationRequest(any())).
		thenReturn(Mono.just(new UserAccreditationResponse(Boolean.FALSE)));
		
		UserAccreditationRequest accreditationRequest = new UserAccreditationRequest("", null);
		
		client.
			post().
			uri("/user/accreditation").accept(MediaType.APPLICATION_JSON).
			bodyValue(accreditationRequest).
			exchange().
			expectStatus().isOk().
			expectHeader().contentType(MediaType.APPLICATION_JSON).
			expectBody().
			jsonPath("$.success").isBoolean().
			jsonPath("$.success").isEqualTo(Boolean.TRUE).
			jsonPath("$.accredited").isBoolean().
			jsonPath("$.accredited").isEqualTo(Boolean.FALSE);
	}
	
	@Test
	public void testAccreditation_AccreditaitonError_ShouldFail() {
		when(service.doProcessAccreditationRequest(any())).
		thenThrow(new IllegalArgumentException("Failed"));
		
		UserAccreditationRequest accreditationRequest = new UserAccreditationRequest("", null);
		
		client.
			post().
			uri("/user/accreditation").accept(MediaType.APPLICATION_JSON).
			bodyValue(accreditationRequest).
			exchange().
			expectStatus().is5xxServerError(); 
		/* Expect 500 since we do not have an error handler implementation here, but we do expect
		 * the mock to be hit */
		
		verify(errorHandler, times(2)).throwableError(any());
	}
}
