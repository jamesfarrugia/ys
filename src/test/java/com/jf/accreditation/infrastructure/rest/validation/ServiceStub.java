package com.jf.accreditation.infrastructure.rest.validation;

import org.springframework.web.reactive.function.server.ServerResponse;

import reactor.core.publisher.Mono;

public interface ServiceStub {
	public Mono<ServerResponse> stubProcessor();
}
