package com.jf.accreditation.infrastructure.rest.handler;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.validation.Validator;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.jayway.jsonpath.JsonPath;
import com.jf.accreditation.domain.model.UserAccreditationRequest;
import com.jf.accreditation.domain.model.UserAccreditationResponse;
import com.jf.accreditation.domain.service.UserAccreditationService;
import com.jf.accreditation.testutil.ServerResponseExtractor;

import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class UserAccreditationHandlerTest {
	@Mock 
	private Validator validator;
	@Mock 
	private ErrorHandler errorHandler;
	@Mock 
	private UserAccreditationService userAccreditationService;
	
	@InjectMocks
	private UserAccreditationHandler sut;
	
	@Test
	public void testProcessRequest_SuccessfulAccreditation_ShouldPass() {
		UserAccreditationRequest requestModel = new UserAccreditationRequest("user", null);
		UserAccreditationResponse responseModel = new UserAccreditationResponse(Boolean.TRUE);
		Mono<UserAccreditationResponse> responseMono = Mono.just(responseModel);

		when(userAccreditationService.doProcessAccreditationRequest(Mockito.any())).thenReturn(responseMono);
		ServerRequest sr = Mockito.mock(ServerRequest.class);
		/* null payload as it's out of scope */
		Mono<UserAccreditationRequest> request = Mono.just(requestModel);
		/* Server request is not used */
		Mono<ServerResponse> response = sut.processRequest(request, sr);
		
		StepVerifier.create(response).expectNextMatches(resp -> {
			String responseString = ServerResponseExtractor.serverResponseAsString(resp);

			return resp.statusCode().is2xxSuccessful() && 
					JsonPath.parse(responseString).read("$.success").equals(Boolean.TRUE) &&
					JsonPath.parse(responseString).read("$.accredited").equals(Boolean.TRUE);
		}).verifyComplete();
		
		verify(userAccreditationService, times(1)).doProcessAccreditationRequest(requestModel);
	}
}
