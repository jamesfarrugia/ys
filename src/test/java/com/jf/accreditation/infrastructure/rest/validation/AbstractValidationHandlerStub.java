package com.jf.accreditation.infrastructure.rest.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Validator;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.jf.accreditation.infrastructure.rest.handler.ErrorHandler;

import reactor.core.publisher.Mono;

public class AbstractValidationHandlerStub extends AbstractValidationHandler<RequestStub, Validator> {

	private ServiceStub service;
	
	protected AbstractValidationHandlerStub(
			@Autowired Validator validator,
			@Autowired ErrorHandler errorHandler,
			@Autowired ServiceStub service) {
		super(RequestStub.class, validator, errorHandler);
		this.service = service;
	}

	@Override
	protected Mono<ServerResponse> processRequest(Mono<RequestStub> body, ServerRequest originalRequest) {
		return service.stubProcessor();
	}

}
