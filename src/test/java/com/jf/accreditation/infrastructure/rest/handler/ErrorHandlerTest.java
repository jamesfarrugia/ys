package com.jf.accreditation.infrastructure.rest.handler;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.jayway.jsonpath.JsonPath;
import com.jf.accreditation.testutil.ServerResponseExtractor;

import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class ErrorHandlerTest {
	
	private ErrorHandler sut;
	
	@BeforeEach
	public void beforeEach() {
		this.sut = new ErrorHandler();
	}
	
	@Test
	public void testHandler_UnmappedThrowable_ShouldReplyWithInternalErrorResponse() {
		Mono<ServerResponse> response = this.sut.throwableError(new Exception("Unexpected"));
		
		StepVerifier.create(response).expectNextMatches(resp -> {
			return resp.statusCode().is5xxServerError();
		}).verifyComplete();
	}
	
	@Test
	public void testHandler_IllegalArgumentException_ShouldReplyWithRequestErrorResponse() {
		Mono<ServerResponse> response = this.sut.throwableError(new IllegalArgumentException("Bad arg"));
		
		StepVerifier.create(response).expectNextMatches(resp -> {
			String responseString = ServerResponseExtractor.serverResponseAsString(resp);

			return resp.statusCode().is4xxClientError() &&
					JsonPath.parse(responseString).read("$.success").equals(Boolean.FALSE) &&
					JsonPath.parse(responseString).read("$.message").equals("Bad arg");
		}).verifyComplete();
	}
}
