package com.jf.accreditation.infrastructure.rest.validation;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.jf.accreditation.infrastructure.rest.handler.ErrorHandler;

import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@ExtendWith(MockitoExtension.class)
public class AbstractValidationHandlerTest {
	@Mock 
	private Validator validator;
	@Mock 
	private ErrorHandler errorHandler;
	@Mock
	private ServiceStub service;
	
	@InjectMocks
	private AbstractValidationHandlerStub sut;
	
	@Test
	public void testHandleRequest_SuccessfulResponse_ShouldPass() {
		RequestStub requestModel = new RequestStub();
		ServerResponse mockResponse = Mockito.mock(ServerResponse.class);
		Mono<ServerResponse> responseMono = Mono.just(mockResponse);
		
		ServerRequest sr = Mockito.mock(ServerRequest.class);
		when(sr.bodyToMono(RequestStub.class)).thenReturn(Mono.just(requestModel));
		when(service.stubProcessor()).thenReturn(responseMono);
		
		Mono<ServerResponse> response = sut.handleRequest(sr);
		
		StepVerifier.create(response).expectNextMatches(resp -> {
			return resp.equals(mockResponse);
		}).verifyComplete();
		
		verify(service, times(1)).stubProcessor();
	}
	
	@Test
	public void testHandleRequest_UnsuccessfulResponse_ShouldFail() {
		RequestStub requestModel = new RequestStub();
		
		ServerRequest sr = Mockito.mock(ServerRequest.class);
		when(sr.bodyToMono(RequestStub.class)).thenReturn(Mono.just(requestModel));
		when(service.stubProcessor()).thenReturn(Mono.error(new IllegalArgumentException()));

		doAnswer(invocation -> {
			return null;
		}).when(validator).validate(any(), any());
		
		Mono<ServerResponse> response = sut.handleRequest(sr);
		
		StepVerifier.create(response).expectError().verify();
		
		verify(validator, times(1)).validate(any(), any());
		verify(service, times(1)).stubProcessor();
		verify(errorHandler, times(1)).throwableError(any());
	}
	
	@Test
	public void testHandleRequest_FailedValidation_ShouldFail() {
		RequestStub requestModel = new RequestStub();
		
		ServerRequest sr = Mockito.mock(ServerRequest.class);
		when(sr.bodyToMono(RequestStub.class)).thenReturn(Mono.just(requestModel));

		doAnswer(invocation -> {
			BeanPropertyBindingResult err = invocation.getArgument(1);
			err.addError(new FieldError("test", "test", "failed validation"));
			return null;
		}).when(validator).validate(any(), any());
		
		Mono<ServerResponse> response = sut.handleRequest(sr);
		
		StepVerifier.create(response).expectError().verify();
		
		verify(validator, times(1)).validate(any(), any());
		verify(service, times(0)).stubProcessor();
		verify(errorHandler, times(1)).throwableError(any());
	}
}
