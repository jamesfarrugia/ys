package com.jf.accreditation.infrastructure.rest.validation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class Base64ValidatorTest {

	@Test
	public void testValidation_emptyString_shouldPass() {
		Base64Validator validator = new Base64Validator();
		assertTrue(validator.isValid("", null));
	}
	
	@Test
	public void testValidation_validString_shouldPass() {
		Base64Validator validator = new Base64Validator();
		assertTrue(validator.isValid("ICAiQC8qIjogWyJzcmMvKiJdCiAgICB9CiAgfQp9Cg==", null));
	}
	
	@Test
	public void testValidation_invalidString_shouldFail() {
		Base64Validator validator = new Base64Validator();
		assertFalse(validator.isValid("%%!", null));
	}
}
