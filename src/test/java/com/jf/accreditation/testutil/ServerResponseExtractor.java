package com.jf.accreditation.testutil;

import java.util.Collections;
import java.util.List;

import org.springframework.http.codec.HttpMessageWriter;
import org.springframework.mock.http.server.reactive.MockServerHttpRequest;
import org.springframework.mock.http.server.reactive.MockServerHttpResponse;
import org.springframework.mock.web.server.MockServerWebExchange;
import org.springframework.web.reactive.function.server.HandlerStrategies;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.reactive.result.view.ViewResolver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ServerResponseExtractor {

	public static <T> T serverResponseAsObject(ServerResponse serverResponse, ObjectMapper mapper, Class<T> type) {
		String response = serverResponseAsString(serverResponse);
		try {
			return mapper.readValue(response, type);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	public static String serverResponseAsString(ServerResponse serverResponse) {
		MockServerWebExchange exchange = MockServerWebExchange.from(MockServerHttpRequest.get("/dummy"));

		DebugServerContext debugServerContext = new DebugServerContext();
		serverResponse.writeTo(exchange, debugServerContext).block();

		MockServerHttpResponse response = exchange.getResponse();
		return response.getBodyAsString().block();

	}

	private static class DebugServerContext implements ServerResponse.Context {
		@Override
		public List<HttpMessageWriter<?>> messageWriters() {
			return HandlerStrategies.withDefaults().messageWriters();
		}

		@Override
		public List<ViewResolver> viewResolvers() {
			return Collections.emptyList();
		}
	}
}