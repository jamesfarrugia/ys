package com.jf.accreditation.domain.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;

@JsonTest
public class UserAccreditationResponseTest {

	@Autowired
	private JacksonTester<UserAccreditationResponse> json;
	
	@Test
	public void testSerialize() throws Exception {
		UserAccreditationResponse sut = new UserAccreditationResponse(Boolean.TRUE);
		
		JsonContent<UserAccreditationResponse> result = this.json.write(sut);

		assertThat(result).hasJsonPathBooleanValue("$.accredited");
		assertThat(result).extractingJsonPathBooleanValue("$.accredited").isEqualTo(Boolean.TRUE);
		
		assertThat(result).hasJsonPathBooleanValue("$.success");
		assertThat(result).extractingJsonPathBooleanValue("$.success").isEqualTo(Boolean.TRUE);
	}
	
	@Test
	public void testDeserialize() throws Exception {
		String jsonInput = "{\n"
				+ "\"success\": true,\n"
				+ "\"accredited\": true\n"
				+ "}";
		
		UserAccreditationResponse result = this.json.parse(jsonInput).getObject();

		assertThat(result.getAccredited()).isEqualTo(Boolean.TRUE);
		assertThat(result.isSuccess()).isTrue();
	}
}
