package com.jf.accreditation.domain.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;

import com.jf.accreditation.domain.model.UserAccreditationRequest.AccreditationDocument;
import com.jf.accreditation.domain.model.UserAccreditationRequest.AccreditationPayload;

@JsonTest
public class UserAccreditationRequestTest {

	@Autowired
	private JacksonTester<UserAccreditationRequest> json;
	
	@Test
	public void testSerialize() throws Exception {
		UserAccreditationRequest sut = new UserAccreditationRequest();
		
		String userId = "user";
		String docName = "name";
		String docType = "type";
		String docContent = "content";
		
		Set<AccreditationDocument> documents = new HashSet<AccreditationDocument>();
		documents.add(new AccreditationDocument(docName, docType, docContent));
		AccreditationPayload payload = new AccreditationPayload(AccreditationType.BY_INCOME, documents);
		
		sut.setUserId(userId);
		sut.setPayload(payload);
		
		JsonContent<UserAccreditationRequest> result = this.json.write(sut);

		/* General bits */
		assertThat(result).hasJsonPathStringValue("$.user_id");
		/* test against internal name */
		assertThat(result).doesNotHaveJsonPath("$.userId");
		assertThat(result).extractingJsonPathStringValue("$.user_id").isEqualTo(userId);
		
		/* Payload */
		assertThat(result).hasJsonPathMapValue("$.payload");
		
		/* Payload accred type */
		assertThat(result).hasJsonPathStringValue("$.payload.accreditation_type");
		/* test against internal name */
		assertThat(result).doesNotHaveJsonPath("$.payload.type");
		assertThat(result).extractingJsonPathStringValue("$.payload.accreditation_type").
							isEqualTo(AccreditationType.BY_INCOME.name());
		
		/* Payload docs */
		assertThat(result).hasJsonPathArrayValue("$.payload.documents");
		assertThat(result).hasJsonPathMapValue("$.payload.documents[0]");
		
		/* Payload doc */
		assertThat(result).hasJsonPathStringValue("$.payload.documents[0].name");
		assertThat(result).extractingJsonPathStringValue("$.payload.documents[0].name").isEqualTo(docName);
		
		assertThat(result).hasJsonPathStringValue("$.payload.documents[0].mime_type");
		/* test against internal name */
		assertThat(result).doesNotHaveJsonPath("$.payload.documents[0].mimeType");
		assertThat(result).extractingJsonPathStringValue("$.payload.documents[0].mime_type").isEqualTo(docType);
		
		assertThat(result).hasJsonPathStringValue("$.payload.documents[0].content");
		assertThat(result).extractingJsonPathStringValue("$.payload.documents[0].content").isEqualTo(docContent);
	}
	
	@Test
	public void testDeserialize() throws Exception {
		String jsonInput = "{\n"
				+ "\"user_id\": \"g8NlYJnk7zK9BlB1J2Ebjs0AkhCTpE1V\",\n"
				+ "\"payload\": {\n"
				+ "\"accreditation_type\": \"BY_INCOME\",\n"
				+ "\"documents\": [{\n"
				+ "\"name\": \"2018.pdf\",\n"
				+ "\"mime_type\": \"application/pdf\",\n"
				+ "\"content\": \"ICAiQC8qIjogWyJzcmMvKiJdCiAgICB9CiAgfQp9Cg==\"\n"
				+ "},{\n"
				+ "\"name\": \"2019.jpg\",\n"
				+ "\"mime_type\": \"image/jpeg\",\n"
				+ "\"content\": \"91cy1wcm9taXNlICJeMi4wLjUiCiAgICB0b3Bvc29ydCAiXjIuMC4yIgo=\"\n"
				+ "}]}}";
		
		UserAccreditationRequest result = this.json.parse(jsonInput).getObject();

		AccreditationDocument doc1 = 
				new AccreditationDocument("2018.pdf", "application/pdf", "ICAiQC8qIjogWyJzcmMvKiJdCiAgICB9CiAgfQp9Cg==");
		AccreditationDocument doc2 = 
				new AccreditationDocument("2019.jpg", "image/jpeg", "91cy1wcm9taXNlICJeMi4wLjUiCiAgICB0b3Bvc29ydCAiXjIuMC4yIgo=");
		
		assertThat(result.getUserId()).isEqualTo("g8NlYJnk7zK9BlB1J2Ebjs0AkhCTpE1V");
		assertThat(result.getPayload()).isNotNull();
		assertThat(result.getPayload().getType()).isEqualTo(AccreditationType.BY_INCOME);
		assertThat(result.getPayload().getDocuments().size()).isEqualTo(2);
		assertThat(result.getPayload().getDocuments()).contains(doc1, doc2);
	}
}
