package com.jf.accreditation.domain.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class RandomiserServiceImplTest {

	private RandomiserServiceImpl sut;
	
	@Test
	public void testInitialisation_shouldPass() {
		sut = new RandomiserServiceImpl();
	}
	
	@Test
	public void testRandomisation_shouldNotBeNull_shouldPass() {
		sut = new RandomiserServiceImpl();
		assertThat(sut.nextBoolean()).isNotNull();
	}
}
