package com.jf.accreditation.domain.service.impl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.jf.accreditation.domain.model.UserAccreditationRequest;
import com.jf.accreditation.domain.model.UserAccreditationResponse;
import com.jf.accreditation.domain.service.MetricsService;
import com.jf.accreditation.domain.service.RandomiserService;

import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class RandomUserAccreditationServiceImplTest {
	
	@Mock
	private MetricsService metricsService;
	@Mock
	private RandomiserService random;
	
	@InjectMocks
	private RandomUserAccreditationServiceImpl sut;
	
	@Test
	public void testAccreditation_PassedAccreditation_ShouldReplyWithApproved() {
		/* Payload is left null as this is not within scope of the random accreditor */
		UserAccreditationRequest request = new UserAccreditationRequest("userId", null);
		
		when(random.nextBoolean()).thenReturn(Boolean.TRUE);
		
		Mono<UserAccreditationResponse> response = sut.doProcessAccreditationRequest(request);
		
		StepVerifier.create(response).expectNextMatches(resp -> {
			return resp.getAccredited().booleanValue();
		}).verifyComplete();
	}
	
	@Test
	public void testAccreditation_FailedAccreditation_ShouldReplyWithRejected() {
		/* Payload is left null as this is not within scope of the random accreditor */
		UserAccreditationRequest request = new UserAccreditationRequest("userId", null);
		
		when(random.nextBoolean()).thenReturn(Boolean.FALSE);
		
		Mono<UserAccreditationResponse> response = sut.doProcessAccreditationRequest(request);
		
		StepVerifier.create(response).expectNextMatches(resp -> {
			return !resp.getAccredited().booleanValue();
		}).verifyComplete();
	}
	
	@Test
	public void testAccreditation_SystemError_ShouldReplyWithError() {
		String accreditationError = "User not found";
		
		/* Payload is left null as this is not within scope of the random accreditor */
		UserAccreditationRequest request = new UserAccreditationRequest("userId", null);
		
		when(random.nextBoolean()).thenThrow(new IllegalArgumentException(accreditationError));
		
		Mono<UserAccreditationResponse> response = sut.doProcessAccreditationRequest(request);
		
		StepVerifier.create(response).expectErrorMessage(accreditationError).verify();
	}
	
	@Test
	public void testAccreditationBehaviour_ShouldRandomiseAndNotifyMetrics() {
		UserAccreditationRequest request = new UserAccreditationRequest("userId", null);
		
		when(random.nextBoolean()).thenReturn(Boolean.TRUE);
		StepVerifier.create(sut.doProcessAccreditationRequest(request)).expectNextCount(1).verifyComplete();
		verify(metricsService, times(1)).onApprovedAccreditation();
		verify(random, times(1)).nextBoolean();
		
		when(random.nextBoolean()).thenReturn(Boolean.FALSE);
		StepVerifier.create(sut.doProcessAccreditationRequest(request)).expectNextCount(1).verifyComplete();
		verify(metricsService, times(1)).onRejectedAccreditation();
		/* already hit so it is time 2 now */
		verify(random, times(2)).nextBoolean();
	}
}
