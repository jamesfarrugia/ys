package com.jf.accreditation.domain.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;

public class PrometheusMetricsServiceImplTest {

	private MeterRegistry registry;
	private List<Tag> approvedTag;
	private List<Tag> rejectedTag;
	
	private PrometheusMetricsServiceImpl sut;
	
	@BeforeEach
	public void before() {
		this.approvedTag = new ArrayList<>();
		approvedTag.add(Tag.of("type", "approved"));
		
		this.rejectedTag = new ArrayList<>();
		rejectedTag.add(Tag.of("type", "rejected"));
	}
	
	@Test
	public void testInitialisation_shouldPass() {
		registry = new SimpleMeterRegistry();
		sut = new PrometheusMetricsServiceImpl(registry);
		assertThat(registry.find("accreditation.requests").counter()).isNotNull();
	}
	
	@Test
	public void testCounting_shouldPass() {
		registry = new SimpleMeterRegistry();
		sut = new PrometheusMetricsServiceImpl(registry);
		
		assertThat(registry.counter("accreditation.requests", approvedTag).count()).isEqualTo(0.0d);
		assertThat(registry.counter("accreditation.requests", rejectedTag).count()).isEqualTo(0.0d);
		
		sut.onApprovedAccreditation();
		assertThat(registry.counter("accreditation.requests", approvedTag).count()).isEqualTo(1.0d);
		
		sut.onRejectedAccreditation();
		assertThat(registry.counter("accreditation.requests", rejectedTag).count()).isEqualTo(1.0d);
	}
}
